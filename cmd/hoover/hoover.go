package main

import (
	"flag"
	"strings"

	"gitlab.com/thorchain/bepswap/smoke-tests/x/hoover"
)

func main() {
	masterKey := flag.String("m", "", "The master key of the wallet to transfer assets to.")
	keyList := flag.String("k", "", "A comma seperated list of keys to hoover assets from.")
	flag.Parse()

	keys := strings.Split(*keyList, ",")

	h := hoover.NewHoover(*masterKey, keys)
	h.EmptyWallets()
}
