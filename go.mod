module gitlab.com/thorchain/bepswap/smoke-tests

go 1.12

require (
	github.com/binance-chain/go-sdk v1.1.2
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/go-resty/resty/v2 v2.0.0
	gopkg.in/check.v1 v0.0.0-20161208181325-20d25e280405
)
