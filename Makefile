DOCKER_IMAGE=registry.gitlab.com/thorchain/smoke-testnet:latest
ACTOR_KEYS=/data/.keys
MASTER_KEY?=MASTER_KEY
POOL_ADDR?=POOL_ADDR

build:
	docker build -t ${DOCKER_IMAGE} .

hoover:
	docker run --rm -v smoke-volume:/data --privileged ${DOCKER_IMAGE} /scripts/hoover.sh ${ACTOR_KEYS} ${MASTER_KEY}

smoke:
	docker run --rm -v smoke-volume:/data ${DOCKER_IMAGE} smoke -m ${MASTER_KEY} -p ${POOL_ADDR} -c /smoke/stake.json -f ${ACTOR_KEYS}
	docker run --rm -v smoke-volume:/data ${DOCKER_IMAGE} smoke -m ${MASTER_KEY} -p ${POOL_ADDR} -c /smoke/swap.json -f ${ACTOR_KEYS}
	docker run --rm -v smoke-volume:/data ${DOCKER_IMAGE} smoke -m ${MASTER_KEY} -p ${POOL_ADDR} -c /smoke/withdraw.json -f ${ACTOR_KEYS}
	docker run --rm -v smoke-volume:/data ${DOCKER_IMAGE} smoke -m ${MASTER_KEY} -p ${POOL_ADDR} -c /smoke/stake.json -f ${ACTOR_KEYS}
