# Smoke tests THORNode Testnet

Smoke tests to run against THORNode Testnet to create volume and transactions.

Requirements:
- Master account private key of wallet toseed tmp accounts that will generate
	transactions in Binance testnet.
- Pool address on THORNode Testnet, eg. tbnb1mfmgxrtxluh0ee6r6swx7t4ty3drtkg47lw8na

Command availables:
- build: build docker image
- smoke: master account seeding coins to temporary accountsgenerating transactions
- hoover: return coins from temporary accounts to master account

Usual flow for THORNode Testnet from scratch to ready for QA to renew would be:

1) Start THORNode testnet from *THORNode repository* using make commands, eg. for standalone:

	`make -C build/docker run-testnet-standalone`

2) Run smoke command from this repo to generate volumes and pools.

	`MASTER_KEY=my_wallet_private_key POOL_ADDR=tbnb1mfmgxrtxluh0ee6r6swx7t4ty3drtkg47lw8na make smoke`

3) QA team to use Testnet and doo real world testing

4) Trigger refund of the pools from *THORNode repository* using make command:

	`make -C build/docker refund-testnet`

5) Hoover funds back to master wallet from tmp accounts:

	`MASTER_KEY=my_wallet_master_key make hoover`


## Build

Requirements:
- make
- docker

Build docker image to be able to run commands.

`make build`


## Smoke

Command to run smoke tests. That command will use the master wallet account to
first seed multiple temporary accounts: admin, user, staker_1, staker_2.
The command will then use those temporary account to generate transactions in
THORNode pool by triggering transaction on Binance testnet.
Needs environment variables to set up master private key wallet and pool address.

`MASTER_KEY=my_wallet_private_key POOL_ADDR=tbnb1mfmgxrtxluh0ee6r6swx7t4ty3drtkg47lw8na make smoke`

## Hoover

Command to "hoover" funds back from the temporary account wallets back to the
master account wallet. Usually this command would be run after triggering a refund
of the pools in THORNode back to the staker wallets, here our temporary accounts.

`MASTER_KEY=my_wallet_private_key make hoover`
